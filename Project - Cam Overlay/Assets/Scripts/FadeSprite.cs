using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeSprite : MonoBehaviour {

    [SerializeField] List<SpriteRenderer> renderers = new List<SpriteRenderer> ();
    List<Color> originalColours = new List<Color> ();

    void OnMouseEnter () {
        originalColours.Clear ();
        for (var i = 0; i < renderers.Count; i++) {
            originalColours.Add (renderers[i].color);
            Color colour = renderers[i].color;
            colour.a = 0.1f + (1 - colour.a);
            renderers[i].color = colour;
        }
    }

    void OnMouseExit () {
        for (var i = 0; i < renderers.Count; i++) {
            renderers[i].color = originalColours[i];
        }
    }
}