using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayWebCam : MonoBehaviour {
    // [SerializeField] RawImage rawImage;
    [SerializeField] SpriteRenderer spriteRenderer;

    int cameraIndex = 0;
    WebCamTexture tex;

    Vector3 originalScale;

    void Start () {
        originalScale = transform.localScale;

        MoveToBottomRight ();
        ActivateCamera ();
    }

    void MoveToBottomRight () {
        transform.position = Camera.main.ViewportToWorldPoint (new Vector3 (0.977f, 0.05f, 10));
    }

    void Update () {
        if (Application.isFocused) {
            if (Input.GetKeyDown (KeyCode.KeypadPlus)) {
                //Change desired cam index
                int deviceLength = WebCamTexture.devices.Length;
                cameraIndex++;
                if (cameraIndex >= deviceLength) {
                    cameraIndex = 0;
                }
                ActivateCamera ();
            }
        }

        if (Input.GetKeyDown (KeyCode.F9) && (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl))) {
            Debug.Log ($"Toggle Scale");
            if (transform.localScale != Vector3.zero) {
                transform.localScale = Vector3.zero;
            } else {
                transform.localScale = originalScale;
            }
        }
    }

    void ActivateCamera () {
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0) return;

        // for debugging purposes, prints available devices to the console
        for (int i = 0; i < devices.Length; i++) {
            Debug.Log ("Webcam available: " + devices[i].name);
        }

        if (tex != null && tex.isPlaying) tex.Stop ();

        tex = new WebCamTexture (devices[cameraIndex].name, 1280, 720);
        spriteRenderer.material.SetTexture ("_OverlayTex", tex);
        // rawImage.texture = tex;
        if (tex.isReadable) tex.Play ();
        else Debug.Log ("Webcam unreadable");
    }

}